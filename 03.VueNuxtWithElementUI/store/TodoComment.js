export const state = () => ({
    comment: ''
});

export const mutations = {
    update(state, data) {
        state[data.itemValue] = data.currentValue;
    }
};